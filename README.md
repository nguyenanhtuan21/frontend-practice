# frontend-practice
# frontend-practice
# Bài tập Trainee Colombo 2019


## Chuyển từ psd sang html


Tạo 1 trang html từ file PSD cho trước


Thực hiện bởi [Nguyễn Ngọc Sơn](https://github.com/sonbeo1999hd)
# Liên kết
- github page: https://sonbeo1999hd.github.io/frontend-practice/index.html
- PSD:https://github.com/sonbeo1999hd/frontend-practice/blob/master/images/2457348.psd
## Kiến thức nắm được


- Củng cố kiến thức HTNL/CSS/JS

- Tạo 1 trang html cơ bản từ PSD, không JS, không Responsive
